// initialize node package manager
// npm init -y

// npm install express mongoose

/*
	Mini-Activity
		Create an expressJS designated to port 4000
		Create a new route with endpoint /hello and method GET 
			- Should be able to respond with "Hello World"
*/

// express creates a server
const express = require("express");

// Mongoose is a package that allows creation of schema to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
app.use(express.json());
const port = 4000

app.get("/hello", (req,res) => {
	res.send("Hello World");
})

// MongoDB Connection 
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>")

	Sample Connection String:
		"mongodb+srv://<adminUserName>:<password>@batch204-yatcodaphne.rlp0fjo.mongodb.net/<collectionName>?retryWrites=true&w=majority"	
*/
mongoose.connect("mongodb+srv://admin:admin123@batch204-yatcodaphne.rlp0fjo.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
);

// Set notifications for connection success or failure
let db = mongoose.connection

// If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error")); 

// If connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"))

/*// Mongoose Schema
// Schema determine structure of documents to be written in database
// Schema act as blueprints to our data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Models
// First parameter represents collection name where we store data
// Second parameter specifies schema of documents that will be stored in MongoDB collection
const Task = mongoose.model("Task", taskSchema);

// ROUTE to create a Task
app.post("/tasks", (req, res) => {

	console.log(req.body);

// req.body.name is the contents of body in postman
// checks if there is duplicate
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result !== null && result.name == req.body.name){
			return res.send("Duplicate Task Found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task created");
				}
			}) 
		}
	});
});

// Get all tasks

app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if (err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}

	})
})*/

// ACTIVITY

// SCHEMA
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// MODEL
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {

	console.log(req.body);

// req.body.name is the contents of body in postman
// checks if there is duplicate
	User.findOne({username: req.body.username}, (err, result) => {
		if (result !== null && result.username == req.body.username){
			return res.send("Duplicate User Found");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New User registered");
				}
			}) 
		}
	});
});

app.listen(port, () => console.log(`Server is running at port: ${port}`));